<?php declare(strict_types=1);

namespace ValueSuggestAny\Suggester\JsonLd;

use Laminas\Http\Client;
use ValueSuggest\Suggester\SuggesterInterface;

class JsonLdSuggester implements SuggesterInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $config;

    public function __construct(Client $client, array $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    public function getSuggestions($query, $lang = null)
    {
        $query = trim($query);
        if (!strlen($query)) {
            return [];
        }

        if ($lang) {
            $request = str_replace(
                ['__LANG__', '__QUERY__'],
                [rawurlencode($lang), rawurlencode($query)],
                $this->config['url']
            );
        } else {
            $request = str_replace(['__QUERY__'], [rawurlencode($query)], $this->config['url']);
        }

        $pos = mb_strpos($request, '?');
        $requestUri = mb_substr($request, 0, $pos);
        $requestQuery = mb_substr($request, $pos +1);

        $requestParams = [];
        parse_str($requestQuery, $requestParams);

        // Remove the lang if arg is set in url but not in query.
        if (!$lang && $pos = array_search('__LANG__', $requestParams)) {
            unset($requestParams[$pos]);
        }

        $response = $this->client
            ->setUri($requestUri)
            ->setParameterGet($requestParams)
            ->send();
        if (!$response->isSuccess()) {
            return [];
        }

        // Parse the JSON response.
        $suggestions = [];
        $results = json_decode($response->getBody(), true);

        if ($this->config['resources_root']) {
            $results = $results[$this->config['resources_root']] ?? [];
        }

        $useIdUrl = !empty($this->config['id_url']);
        $idKey = $this->config['id_key'] ?: [
            'http://purl.org/dc/terms/identifier',
            'dcterms:identifier',
        ];

        $labelKey = $this->config['label_key'];
        $labelKeys = $labelKey ? [$labelKey] : null;

        $infoKey = $this->config['info_key'];
        $infoKeys = $infoKey ? [$infoKey] : null;

        // Get value with the defined language, the queried one, or the first.
        // Some vocabularies use skos:definition, some others dcterms:description,
        // so a function manages them all.
        $getValue = function ($result, $terms, ?array $flatArray = null) use ($lang): ?string {
            if (!is_array($terms)) {
                $terms = [$terms];
            }
            foreach ($terms as $term) {
                if (is_null($term) || !strlen((string) $term)) {
                    continue;
                }
                // Try to use a flat array first and object notation.
                $values = null;
                if ($flatArray) {
                    if (empty($flatArray[$term])) {
                        continue;
                    }
                    $values = $flatArray[$term];
                } elseif (!empty($result[$term])) {
                    $values = $result[$term];
                } else {
                    continue;
                }
                if (!is_array($values)) {
                    return (string) $values;
                }
                if ($this->config['language']) {
                    foreach ($values as $value) {
                        if (!is_array($value)) {
                            return (string) $value;
                        }
                        if (!empty($value['@language']) && $this->config['language'] === $value['@language']) {
                            return (string) $value['@value'];
                        }
                    }
                }
                if ($lang) {
                    foreach ($values as $value) {
                        if (!is_array($value)) {
                            return (string) $value;
                        }
                        if (!empty($value['@language']) && $lang === $value['@language']) {
                            return (string) $value['@value'];
                        }
                    }
                }
                $value = reset($values);
                return $value = is_string($value) ? $value : (string) $value['@value'];
            }
            return null;
        };

        // TODO Translate default label.
        $defaultLabel = '[No value]';

        $useFlatArray = $labelKey || $infoKey;

        foreach ($results as $result) {
            $flatArray = $useFlatArray ? $this->flatArray($result) : null;
            $value = $getValue($result, $labelKeys ?: [
                'http://www.w3.org/2004/02/skos/core#prefLabel',
                'skos:prefLabel',
                'prefLabel',
                'http://purl.org/dc/terms/title',
                'dcterms:title',
                'o:title',
                'title',
                'label',
            ], $flatArray);
            $info = $getValue($result, $infoKeys ?: [
                'http://www.w3.org/2004/02/skos/core#definition',
                'http://purl.org/dc/terms/description',
                'skos:definition',
                'definition',
                'dcterms:description',
                'description',
            ], $flatArray);
            $scopeNote = $getValue($result, [
                'http://www.w3.org/2004/02/skos/core#scopeNote',
                'skos:scopeNote',
                'scopeNote',
            ]);
            if ($scopeNote) {
                /* // Value Suggest info doesn't support tags in default vesion.
                $info .=  "\n<div><i>strong>"
                    . 'Scope note' // @translate
                    . '</strong>: '
                    . $scopeNote
                    . '</i></div>';
                */
                $info .=  "\n"
                    . 'Scope note:' // @translate
                    . ' ' . $scopeNote;
            }

            $id = $useIdUrl ? $getValue($result, $idKey) : null;
            if (empty($id)) {
                if (empty($result['@id'])) {
                    continue;
                }
                $uri = $result['@id'];
            } else {
                $uri = str_replace('__ID__', $id, $this->config['id_url']);
            }

            $suggestions[] = [
                // Return empty string when no value: ValueSuggest has issue with null.
                'value' => is_null($value) || !strlen($value) ? $defaultLabel : $value,
                'data' => [
                    'uri' => $uri,
                    'info' => $info,
                ],
            ];
        }

        return $suggestions;
    }

    /**
     * Create a flat array from a recursive array.
     *
     * @example
     * ```
     * // The following recursive array:
     * [
     *     'video' => [
     *         'data.format' => 'jpg',
     *         'creator' => ['alpha', 'beta'],
     *     ],
     * ]
     * // is converted into:
     * [
     *     'video.data\.format' => 'jpg',
     *     'creator.0' => 'alpha',
     *     'creator.1' => 'beta',
     * ]
     * ```
     *
     * @see \AdvancedResourceTemplate\Mvc\Controller\Plugin\ArtMapper::flatArray()
     * @see \BulkImport\Mvc\Controller\Plugin\MetaMapper::flatArray()
     * @todo Factorize flatArray() between modules.
     */
    protected function flatArray(?array $array): array
    {
        // Quick check.
        if (empty($array)) {
            return [];
        }
        if (array_filter($array, 'is_scalar') === $array) {
            return $array;
        }
        $flatArray = [];
        $this->_flatArray($array, $flatArray);
        return $flatArray;
    }

    /**
     * Recursive helper to flat an array with separator ".".
     *
     * @todo Find a way to keep the last level of array (list of subjects…), currently managed with fields.
     */
    private function _flatArray(array &$array, array &$flatArray, ?string $keys = null): void
    {
        foreach ($array as $key => $value) {
            $nKey = str_replace('.', '\.', $key);
            if (is_array($value)) {
                $this->_flatArray($value, $flatArray, $keys . '.' . $nKey);
            } else {
                $flatArray[trim($keys . '.' . $nKey, '.')] = $value;
            }
        }
    }
}
