<?php declare(strict_types=1);

namespace ValueSuggestAny\Service\DataType;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use ValueSuggestAny\DataType\JsonLd\JsonLd;

class JsonLdFactory implements AbstractFactoryInterface
{
    /**
     * @var array
     */
    protected $thesaurus;

    public function canCreate(ContainerInterface $services, $requestedName)
    {
        if (is_null($this->thesaurus)) {
            $this->thesaurus = $services->get('Omeka\Settings')->get('valuesuggestany_thesaurus', []);
        }
        // Get the name from the requested name (20 characters for valuesuggest:jsonld:).
        if (substr($requestedName, 0, 20) !== 'valuesuggest:jsonld:') {
            return false;
        }
        return isset($this->thesaurus[mb_substr($requestedName, 20)]);
    }

    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        // This is an abstract factory, so method canCreate() is called first
        // and the requested name is already checked.
        $name = mb_substr($requestedName, 20);
        return new JsonLd(
            $services,
            $name,
            $this->thesaurus[$name]
        );
    }
}
