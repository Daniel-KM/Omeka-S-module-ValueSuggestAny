<?php declare(strict_types=1);

namespace ValueSuggestAny\DataType\JsonLd;

use Laminas\ServiceManager\ServiceManager;
use ValueSuggestAny\Suggester\JsonLd\JsonLdSuggester;
use ValueSuggest\DataType\AbstractDataType;

class JsonLd extends AbstractDataType
{
    /**
     * @var ServiceManager
     */
    protected $services;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $config;

    public function __construct(ServiceManager $services, string $name, array $config)
    {
        $this->services = $services;
        $this->name = $name;
        $this->config = $config + [
            'url' => null,
            'id_key' => null,
            'id_url' => null,
            'language' => null,
            'label' => null,
            'label_key' => null,
            'info_key' => null,
        ];
    }

    public function getName()
    {
        return 'valuesuggest:jsonld:' . $this->name;
    }

    public function getLabel()
    {
        return empty($this->config['label']) ? $this->name : $this->config['label'];
    }

    public function getOptgroupLabel()
    {
        return 'Value Suggest: json-ld'; // @translate
    }

    public function getSuggester()
    {
        return new JsonLdSuggester(
            $this->services->get('Omeka\HttpClient'),
            $this->config
        );
    }
}
