<?php declare(strict_types=1);

namespace ValueSuggestAny\Form;

use Laminas\Form\Fieldset;
use ValueSuggestAny\Form\Element\ConfigIniTextarea;

class SettingsFieldset extends Fieldset
{
    /**
     * @var string
     */
    protected $label = 'Value Suggest: Any'; // @translate

    protected $elementGroups = [
        'resources' => 'Resources', // @translate
    ];

    public function init(): void
    {
        $this
            ->setAttribute('id', 'value-suggest-any')
            ->setOption('element_groups', $this->elementGroups)
            ->add([
                'name' => 'valuesuggestany_thesaurus',
                'type' => ConfigIniTextarea::class,
                'options' => [
                    'element_group' => 'resources',
                    'label' => 'Json-ld endpoints for Value Suggest Any', // @translate
                    'info' => 'Each endpoint should have at least a name between "[]" and a url, and optionally an base url for id, a language code and a label.', // @translate
                    'documentation' => 'https://github.com/Daniel-KM/omeka-s-module-ValueSuggestAny',
                    'as_key_value' => true,
                ],
                'attributes' => [
                    'id' => 'valuesuggestany_thesaurus',
                    'rows' => 10,
                    'placeholder' => '[opentheso-savoirs]
url = "https://opentheso.huma-num.fr/opentheso/api/search?theso=th184&format=jsonld&lang=__LANG__&q=__QUERY__"
id_url = "https://opentheso.huma-num.fr/opentheso/?idt=th184&idc=__ID__"
language = "fr"
label = "Savoirs"

[omeka-sandbox]
url = "https://dev.omeka.org/omeka-s-sandbox/api/items?property[0][joiner]=and&property[0][property]=&property[0][type]=in&property[0][text]=__QUERY__"
'
                ],
            ]);
    }
}
