<?php declare(strict_types=1);

namespace ValueSuggestAny\Form\Element;

use Laminas\Config\Reader\Ini as IniReader;
use Laminas\Config\Writer\Ini as IniWriter;
use Laminas\Form\Element\Textarea;
use Laminas\InputFilter\InputProviderInterface;

/**
 * Similar to ArrayTextarea, but for a multi-level array via ini representation.
 *
 * Warning: a few characters in keys are not supported.
 */
class ConfigIniTextarea extends Textarea implements InputProviderInterface
{
    /**
     * @see \Laminas\Config\Writer\Ini
     * @var string
     */
    protected $nestSeparator = '.';

    /**
     * @see \Laminas\Config\Writer\Ini (inverse).
     * @var bool
     */
    protected $renderSections = true;

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        parent::setOptions($options);
        if (array_key_exists('nest_separator', $this->options)) {
            $this->setNestSeparator($this->options['nest_separator']);
        }
        if (array_key_exists('render_sections', $this->options)) {
            $this->setRenderSections($this->options['render_sections']);
        }
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $this->arrayToString($value);
        return $this;
    }

    public function getInputSpecification()
    {
        return [
            'name' => $this->getName(),
            'required' => false,
            'allow_empty' => true,
            'filters' => [
                [
                    'name' => \Laminas\Filter\Callback::class,
                    'options' => [
                        'callback' => [$this, 'stringToArray'],
                    ],
                ],
            ],
        ];
    }

    public function arrayToString($array): string
    {
        if (is_string($array)) {
            return $array;
        }
        $writer = new IniWriter;
        try {
            return $writer
                ->setNestSeparator($this->getNestSeparator())
                ->setRenderWithoutSectionsFlags(!$this->getRenderSections())
                ->toString($array);
        } catch (\Laminas\Config\Exception\RuntimeException $e) {
            return '';
        }
    }

    public function stringToArray($string): array
    {
        if (is_array($string)) {
            return $string;
        }
        $reader = new IniReader;
        try {
            return $reader
                ->setNestSeparator($this->getNestSeparator())
                ->fromString($string);
        } catch (\Laminas\Config\Exception\RuntimeException $e) {
            return [];
        }
    }

    /**
     * Set nest separator.
     */
    public function setNestSeparator(string $separator): ConfigIniTextarea
    {
        $this->nestSeparator = $separator;
        return $this;
    }

    /**
     * Get nest separator.
     */
    public function getNestSeparator(): string
    {
        return $this->nestSeparator;
    }

    /**
     * Set if rendering should occur without sections or not.
     */
    public function setRenderSections($renderSections): ConfigIniTextarea
    {
        $this->renderSections = (bool) $renderSections;
        return $this;
    }

    /**
     * Return whether the writer should render without sections.
     */
    public function getRenderSections(): bool
    {
        return $this->renderSections;
    }
}
