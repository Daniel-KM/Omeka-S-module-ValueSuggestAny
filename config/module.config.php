<?php declare(strict_types=1);

namespace ValueSuggestAny;

return [
    'data_types' => [
        'abstract_factories' => [
            Service\DataType\JsonLdFactory::class,
        ],
    ],
    'form_elements' => [
        'invokables' => [
            Form\Element\ConfigIniTextarea::class => Form\Element\ConfigIniTextarea::class,
            Form\SettingsFieldset::class => Form\SettingsFieldset::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'valuesuggestany' => [
        'settings' => [
            'valuesuggestany_thesaurus' => [
            ],
        ],
    ],
];
