<?php declare(strict_types=1);

namespace ValueSuggestAny;

if (!class_exists(\Generic\AbstractModule::class)) {
    require file_exists(dirname(__DIR__) . '/Generic/AbstractModule.php')
        ? dirname(__DIR__) . '/Generic/AbstractModule.php'
        : __DIR__ . '/src/Generic/AbstractModule.php';
}

use Generic\AbstractModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;

class Module extends AbstractModule
{
    const NAMESPACE = __NAMESPACE__;

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        $sharedEventManager->attach(
            \Omeka\DataType\Manager::class,
            'service.registered_names',
            [$this, 'filterRegisteredNames']
        );
        $sharedEventManager->attach(
            \Omeka\Form\SettingForm::class,
            'form.add_elements',
            [$this, 'handleMainSettings']
        );
    }

    public function filterRegisteredNames(Event $event): void
    {
        $thesaurus = $this->getServiceLocator()->get('Omeka\Settings')
            ->get('valuesuggestany_thesaurus', false);
        if (!$thesaurus) {
            return;
        }
        $names = $event->getParam('registered_names');
        foreach (array_keys($thesaurus) as $name) {
            $names[] = 'valuesuggest:jsonld:' . $name;
        }
        $event->setParam('registered_names', $names);
    }
}
