Value Suggest : Any (module pour Omeka S)
=========================================

See [readme].

[Value Suggest : Any] est un module pour [Omeka S] qui permet d’utiliser
n’importe quel service disponible en json-ld pour indexer les ressources via le
module [Value Suggest].

Actuellement, le module a été testé avec les thésaurus fournis par :
- [OpenTheso], un gestionnaire de thésaurus libre utilisé par [Huma-Num] ;
- Omeka lui-même, via le module [Thesaurus], ou avec toute liste d’éléments
  décrits avec le vocabulaire [skos], voire avec un simple titre.


Installation
------------

Ce module nécessite le module [Value Suggest] et optionnellement [Generic].

Voir la documentation générale de l’utilisateur final pour l’[installation d’un module].

* Depuis le zip

Téléchargez la dernière version [ValueSuggestAny.zip] dans la liste des livraisons,
et décompressez-la dans le dossier "modules".

* A partir des sources et pour le développement

Si le module a été installé à partir de la source, renommez le nom du dossier de
le module "ValueSuggestAny".


Démarrage rapide
----------------

Dans les paramètres principaux d’Omeka, vous devez spécifier les thésaurus que
vous souhaitez. Chacun doit avoir au moins un nom indiqué dans "[]" et une url,
et éventuellement l’url à utiliser pour l’id à la place de l’uri rdf, la langue
à utiliser pour son libellé, et le libellé du service.

Par exemple, pour utiliser les données du bac à sable d’Omeka :

```ini
[omeka-sandbox]
url = "https://dev.omeka.org/omeka-s-sandbox/api/items?property[0][joiner]=and&property[0][property]=&property[0][type]=in&property[0][text]=__QUERY__"
```

Pour le thésaurus [Savoirs] d’OpenTheso :

```ini
[opentheso-savoirs]
url = "https://opentheso.huma-num.fr/opentheso/api/search?theso=th184&format=jsonld&lang=__LANG__&q=__QUERY__"
id_url = "https://opentheso.huma-num.fr/opentheso/api/__ID__"
language = "fr"
label = "Savoirs"
```

Note : Avec OpenTheso, selon sa version, l’identifiant du thésaurus peut être
`th1` ou `TH_1`, et c’est sensible à la casse. Vérifiez donc la casse de la
chaîne et le "_".

Le nom entre `[]` est utilisé pour créer le type de données de la valeur. Il est
donc déconseillé de le changer une fois fixé, sinon il ne sera pas possible
d’associer les valeurs existantes avec le nouveau nom, et vous devrez mettre à
jour tous les modèles qui l’utilisent.

Dans l’url, vous pouvez spécifier `__LANG__` pour limiter les résultats à une
langue si l’utilisateur le spécifie dans le formulaire et si le service le prend
en charge, et `__QUERY__`, qui est l’espace réservé pour la requête de
l’utilisateur. Pour plus de détail, cf. l’[aide OpenTheso].

L’id permet de spécifier une url spécifique pour la valeur. C’est normalement la
valeur rdf qui est l’uri canonique, mais elle peut ne pas être une url, donc il
peut ne pas être utilisable dans un site. C’est le cas d’OpenTheso, où de
nombreux thésaurus ont une uri canonique qui n’est pas une url. Pour cela, deux
autres ids peuvent être utilisés : celui di json-ld ou celui du site web. Le
premier est utile pour récupérer les données, mais le second est plus lisible
par les utilisateurs finaux. Le premier est donc recommandé, mais le second peut
être utile dans certains cas. L’exemple ci-dessus est l’url des données json-ld
et `__ID__` est l’espace réservé pour la notice en cours. Pour enregistrer l’url
publique du site web :

```ini
id_url = "https://opentheso.huma-num.fr/opentheso/?idc=__ID__"
```

Ce point est similaire pour Omeka : par défaut, l’uri est une url qui est le
json-ld api, mais si vous voulez l’url web, vous pouvez spécifier la clé de l’id,
c’est-à-dire la clé de la valeur à utiliser comme identifiant, par défaut
"http://purl.org/dc/terms/identifier" ou "dcterms:identifier", et l’url de l’id :

```ini
id_key = "o:id"
id_url = "https://dev.omeka.org/omeka-s-sandbox/s/mallhistory/item/__ID__"
```

La langue permet de sélectionner le libellé de la valeur, dans le cas où le
service supporte le multilinguisme et que le libellé de l’uri est traduit.

Le label est utilisé comme référence, principalement pour le modèle de ressource.

Ensuite, il s’agit de créer ou de mettre à jour un modèle de ressource :
sélectionnez une propriété, puis dans les types de données, choisissez le
thésaurus souhaité.

C’est tout ! Vous pouvez maintenant créer une ressource avec une valeur
provenant du thésaurus que vous avez spécifié.


Attention
---------

Utilisez-le à vos propres risques.

Il est toujours recommandé de sauvegarder vos fichiers et vos bases de données
et de vérifier vos archives régulièrement afin de pouvoir les reconstituer si
nécessaire.


Dépannage
---------

Voir les problèmes en ligne sur la page des [questions du module] du GitLab.


Licence
-------

Ce module est publié sous la licence [CeCILL v2.1], compatible avec [GNU/GPL] et
approuvée par la [FSF] et l’[OSI].

Ce logiciel est régi par la licence CeCILL de droit français et respecte les
règles de distribution des logiciels libres. Vous pouvez utiliser, modifier
et/ou redistribuer le logiciel selon les termes de la licence CeCILL telle que
diffusée par le CEA, le CNRS et l’INRIA à l’URL suivante "http://www.cecill.info".

En contrepartie de l’accès au code source et des droits de copie, de
modification et de redistribution accordée par la licence, les utilisateurs ne
bénéficient que d’une garantie limitée et l’auteur du logiciel, le détenteur des
droits patrimoniaux, et les concédants successifs n’ont qu’une responsabilité
limitée.

À cet égard, l’attention de l’utilisateur est attirée sur les risques liés au
chargement, à l’utilisation, à la modification et/ou au développement ou à la
reproduction du logiciel par l’utilisateur compte tenu de son statut spécifique
de logiciel libre, qui peut signifier qu’il est compliqué à manipuler, et qui
signifie donc aussi qu’il est réservé aux développeurs et aux professionnels
expérimentés ayant des connaissances informatiques approfondies. Les
utilisateurs sont donc encouragés à charger et à tester l’adéquation du logiciel
à leurs besoins dans des conditions permettant d’assurer la sécurité de leurs
systèmes et/ou de leurs données et, plus généralement, à l’utiliser et à
l’exploiter dans les mêmes conditions en matière de sécurité.

Le fait que vous lisez actuellement ce document signifie que vous avez pris
connaissance de la licence CeCILL et que vous en acceptez les termes.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2023 (voir [Daniel-KM] sur GitLab)

Ce module a été construit pour la future bibliothèque numérique du [Campus Condorcet].


[Value Suggest : Any]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny
[readme]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny/-/blob/master/README.md
[Omeka S]: https://omeka.org/s
[OpenTheso]: https://github.com/miledrousset/Opentheso2
[Huma-Num]: https://opentheso.huma-num.fr/opentheso
[Thesaurus]: https://gitlab.com/Daniel-KM/Omeka-S-module-Thesaurus
[skos]: https://www.w3.org/2004/02/skos/
[installation d’un module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[Value Suggest]: https://github.com/Omeka-S-modules/ValueSuggest
[Generic]: https://gitlab.com/Daniel-KM/Omeka-S-module-Generic
[ValueSuggestAny.zip]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny/-/releases
[Savoirs]: https://opentheso.huma-num.fr/opentheso/index.xhtml
[aide OpenTheso]: https://github.com/miledrousset/Opentheso2/raw/master/src/main/resources/install/webservices.pdf
[questions du module]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedResourceTemplate/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[MIT]: http://opensource.org/licenses/MIT
[Campus Condorcet]: https://campus-condorcet.fr
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
