Value Suggest: Any (module for Omeka S)
=======================================

Consulter le [lisezmoi].

[Value Suggest: Any] is a module for [Omeka S] that allows to use any endpoint
service available in json to index resources via the module [Value Suggest].

Currently, the module has been tested with the thesaurii provided by:
- [OpenTheso], an open source thesaurus manager used by [Huma-Num];
- Omeka itself, through the module [Thesaurus], or with any list of items
  described with the [skos] vocabulary, or even with a simple title.


Installation
------------

This module requires the module [Value Suggest] and optionnaly [Generic].

See general end user documentation for [installing a module].

* From the zip

Download the last release [ValueSuggestAny.zip] from the list of releases,
and uncompress it in the `modules` directory.

* From the source and for development

If the module was installed from the source, rename the name of the folder of
the module to `ValueSuggestAny`.


Quick start
-----------

In the main settings of Omeka, you should specify the thesaurus you want. Each
endpoint should have at least a name between `[]` and a url, and optionally the
url to use for id in place of the rdf uri, the language to use for its label,
and the label of the endpoint.

For example, to use the data from the Omeka sandbox, it is:

```ini
[omeka-sandbox]
url = "https://dev.omeka.org/omeka-s-sandbox/api/items?property[0][joiner]=and&property[0][property]=&property[0][type]=in&property[0][text]=__QUERY__"
```

For the thesaurus [Savoirs] of OpenTheso, it is:

```ini
[opentheso-savoirs]
url = "https://opentheso.huma-num.fr/opentheso/api/search?theso=th184&format=jsonld&lang=__LANG__&q=__QUERY__"
id_url = "https://opentheso.huma-num.fr/opentheso/api/th184.__ID__.jsonld"
language = "fr"
label = "Savoirs"
```

Note: With OpenTheso, depending on its version, the id of the thesaurus can be
`th1` or `TH_1` and it is case sensitive. So check the string case and the "_".

The name between `[]` is used to create the data type of the value, so it should
not change once set, else it won't be possible to associate existing values with
the new name, and you will have to update all templates that use it.

In the url, you can specify a `__LANG__` to limit results to a language if the
user specify it in the form, if the endpoint support it, and `__QUERY__`, that
is a placeholder for the user query. For more details, cf. l'[OpenTheso help].

The id allows to specify a specific url for the value. Normally, the rdf value
as a canonical uri, but it may not be a url, so it may not be usable in a public
site. This is the case for OpenTheso, where many thesaurus have a canonical uri
that is not a url. For it, two other ids can be used: the json-ld one or the web
one. The first one is useful to scrape the data, but the second one is more user
friendly. So the first is recommended, but the second may be useful in some
cases. The example above is the url to the json-ld data and `__ID__` is the
placeholder for the current record. To store the uri for the web front, it is:

```ini
id_url = "https://opentheso.huma-num.fr/opentheso/?idt=th184&idc=__ID__"
```

The point is similar for Omeka: by default the uri is an url that is the json-ld
api endpoint, but if you want the web url, you can specify the id key, that is
the key of the value to use as an id, default to "http://purl.org/dc/terms/identifier"
or "dcterms:identifier", and the id url:

```ini
id_key = "o:id"
id_url = "https://dev.omeka.org/omeka-s-sandbox/s/mallhistory/item/__ID__"
```

The language allows to select the label of the value, in the case where the
endpoint supports multilanguage and that the label of the uri is translated.

The label is used as a reference, mainly for the resource template.

When the output is not the list of results but the results are in a sub-keys,
you need to specify it as `resources_root`, for example for the [Digital Hornbostel & Sachs Classification of Musical Instruments]:

```ini
[hsinstruments]
url = "https://vocabs.acdh.oeaw.ac.at/rest/v1/hsinstruments_thesaurus/search?query=__QUERY__"
id_url = "__ID__"
id_key = "uri"
resources_root = "results"
language = "en"
label = "HSinstruments"
```

Note that the search on this vocabulary accept only exact requests (German or English).

If the endpoint is not a standard json-ld, you may need to set the label of the
resource with `label_key`, here to get the language in Swedish on https://kulturnav.org:

```ini
[kulturnav]
url = "https://kulturnav.org/api/search/entity.dataset:a8797483-ff02-4a4c-adf1-b406cbcd6fc2,nativeText:__QUERY__?lang=sv"
resources_root = ""
id_url = "https://kulturnav.org/api/__ID__"
id_key = "uuid"
label_key = "caption.sv"
info_key = "properties.entity\.description.0.value.sv"
```

The info are available through the key `info_key`. The `label_key` and the
`info_key` should output a string. Note that for kulturnav, `entity.description`
is a single key, so, to avoid a collision, the `.` should be escaped with a `\`.

Then, create or update a resource template: select a property, then in the data
types, choose the thesaurus.

That's all! You can create a resources with a value coming from the thesaurus
you specified.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page on GitLab.


License
-------

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

In consideration of access to the source code and the rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors only have limited liability.

In this respect, the risks associated with loading, using, modifying and/or
developing or reproducing the software by the user are brought to the user’s
attention, given its Free Software status, which may make it complicated to use,
with the result that its use is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore encouraged
to load and test the suitability of the software as regards their requirements
in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions of security.
This Agreement may be freely reproduced and published, provided it is not
altered, and that no provisions are either added or removed herefrom.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2023 (see [Daniel-KM] on GitLab)

This module is built for the future digital library of the [Campus Condorcet].


[Value Suggest: Any]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny
[lisezmoi]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny/-/blob/master/LISEZMOI.md
[Omeka S]: https://omeka.org/s
[OpenTheso]: https://github.com/miledrousset/Opentheso2
[Huma-Num]: https://opentheso.huma-num.fr/opentheso
[Thesaurus]: https://gitlab.com/Daniel-KM/Omeka-S-module-Thesaurus
[skos]: https://www.w3.org/2004/02/skos/
[Installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[Value Suggest]: https://github.com/Omeka-S-modules/ValueSuggest
[Generic]: https://gitlab.com/Daniel-KM/Omeka-S-module-Generic
[ValueSuggestAny.zip]: https://gitlab.com/Daniel-KM/Omeka-S-module-ValueSuggestAny/-/releases
[Savoirs]: https://opentheso.huma-num.fr/opentheso/index.xhtml
[OpenTheso help]: https://github.com/miledrousset/Opentheso2/raw/master/src/main/resources/install/webservices.pdf
[Digital Hornbostel & Sachs Classification of Musical Instruments]: https://vocabs.acdh.oeaw.ac.at/hsinstruments_thesaurus
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedResourceTemplate/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[MIT]: http://opensource.org/licenses/MIT
[Campus Condorcet]: https://campus-condorcet.fr
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
